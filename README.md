## Description
  
Microservices with TypeScript, gRPC, API Gateway, and Authentication | by Ahmad Alhourani

## Repositories

- https://gitlab.com/nest-micro-service/micro_service_boilerplate - App Boilerplate

- https://gitlab.com/nest-micro-service/product-micro - Product Micro (gRPC)
- https://gitlab.com/nest-micro-service/order-micro - Order Micro (gRPC)
- https://gitlab.com/nest-micro-service/auth-micro  - Authentication Micro (gRPC)

- https://gitlab.com/nest-micro-service/gateway-micro - API Gateway (HTTP)
- https://gitlab.com/nest-micro-service/grpc-proto - Shared Proto Repository


## Author

- Ahmad Alhourani
